const express = require('express');
const bodyParser = require('body-parser')
const amqp = require('amqplib');
const fetch = require('node-fetch');
const request = require('request');

const config = require('./config')

const app = express();
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))
// parse application/json
app.use(bodyParser.json())
const port = 3000;



(async () => {

    const connection = await amqp.connect(config.rabbitmq);
    const channel = await connection.createChannel();

    channel.assertQueue('aggres_queue', { durable: false });
    channel.assertQueue('client_push_events', { durable: false });
    channel.consume("aggres_queue", async data => {
        let message = JSON.parse(data.content.toString());
        console.log(message);

        let pushevent;


        switch (message.request_type) {
            case 'reservation':
                pushevent = {
                    channel: "booking-" + message.sessionid,
                    event: "reservation_complete",
                    data: message
                }
                break;
            case 'confirm':
                let succeded = message.status.every(i => i.status);

                let key = (succeded ? "confirmed" : "failed")

                fetch(config.databaseinterface + '/event/' + message.eventid + "/status/" + key, {
                    method: "PATCH"
                }).then(r => r.text()).then(r => console.log(r)).catch(e => console.error(e))

                pushevent = {
                    channel: "booking-" + message.sessionid,
                    event: "booking_complete",
                    data: {
                        status: succeded,
                        vendorstatus: message.status,
                        eventid: message.eventid
                    }
                }
                break;

            default:
                console.error("unknown type");
                return;

        }


        channel.sendToQueue("client_push_events", Buffer.from(JSON.stringify(pushevent)))
    }, { noAck: true })

    app.get("/", (req, res) => {
        res.json({ eventapi: true });
    });

    app.post("/event", async (req, res) => {
        console.log(req.body)

        let dbEvent = await (await fetch(config.databaseinterface + '/event', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(req.body)
        })).json()

        res.json(dbEvent)
    });

    app.get("/event/:id", async (req, res) => {
        request(config.databaseinterface + "/event/" + req.params.id).pipe(res);
    })

    app.patch("/event/:id/:method", async (req, res) => {
        request(config.databaseinterface + "/event/" + req.params.id + "/" + req.params.method, {
            method: "patch",
            body: JSON.stringify(req.body),
            headers: { "Content-Type": "application/json" }
        }).pipe(res)
    })

    app.get("/event", async (req, res) => {
        request(config.databaseinterface + "/event").pipe(res);
    })


    app.listen(port, () => console.log(`listening on port ${port}!`));


})()